clear all
close all
clc

% Parameters
syms I_a I_b I_c I_d J_a J_b J_c J_d K_a K_b K_c K_d;

% Variables
q_vect = sym('q_',[4,1]);   % Position
w_vect = sym('w_',[4,1]);   % Velocity
a_vect = sym('a_',[4,1]);   % Acceleration
T_vect = sym('T_',[4,1]);   % Torques

%% Rotation matrices
% Notation: R^x_y := Rxy
Rna = [cos(q_vect(4)) -sin(q_vect(4)) 0; sin(q_vect(4)) cos(q_vect(4)) 0; 0 0 1];
Rab = [cos(q_vect(3)) 0 sin(q_vect(3)); 0 1 0; -sin(q_vect(3)) 0 cos(q_vect(3))];
Rbc = [1 0 0; 0 cos(q_vect(2)) -sin(q_vect(2)); 0 sin(q_vect(2)) cos(q_vect(2))];
Rcd = [cos(q_vect(1)) 0 sin(q_vect(1)); 0 1 0; -sin(q_vect(1)) 0 cos(q_vect(1))];

Rnb = Rna*Rab;
Rnc = Rna*Rab*Rbc;
Rnd = Rna*Rab*Rbc*Rcd;

% Create block-diagonal rotation matrix
R = blkdiag(Rna,Rnb,Rnc,Rnd);

%% Principal inertia tensors
% Notation: I^A_A := Iaa
Iaa = [I_a,0,0;0,J_a,0;0,0,K_a];
Ibb = [I_b,0,0;0,J_b,0;0,0,K_b];
Icc = [I_c,0,0;0,J_c,0;0,0,K_c];
Idd = [I_d,0,0;0,J_d,0;0,0,I_d]; % I_d = K_d

% Create block-diagonal inertia matrix
It = blkdiag(Iaa,Ibb,Icc,Idd);

%% Jacobians
% Notation: Z^x_y := Zxy
%           J^x_{y,k} := Jxyk
ex = sym([1;0;0]);
ey = sym([0;1;0]);
ez = sym([0;0;1]);
e0 = sym([0;0;0]);

Znn = sym([0;0;1]);
Yna = Rna*ey;
Xnb = Rnb*ex;
Ync = Rnc*ey;

Jnna = [e0, e0, e0, Znn];
Jnnb = [e0, e0, Yna, Znn];
Jnnc = [e0, Xnb, Yna, Znn];
Jnnd = [Ync, Xnb, Yna, Znn];
Jw = [Jnna;Jnnb;Jnnc;Jnnd];


%% Inertia matrix M(q)
RtJw = simplify((R.')*Jw);

% Total Inertia matrix
M = simplify((RtJw.')*It*RtJw);

% Inertia matrices w.r.t. frame i
Ma = simplify((Jnna.')*Rna*Iaa*(Rna.')*Jnna);
Mb = simplify((Jnnb.')*Rnb*Ibb*(Rnb.')*Jnnb);
Mc = simplify((Jnnc.')*Rnc*Icc*(Rnc.')*Jnnc);
Md = simplify((Jnnd.')*Rnd*Idd*(Rnd.')*Jnnd);


%% Coriolis matrix
Gamma = sym(zeros(4,4,4));      % Christoffel symbols
C = sym(zeros(4,4));            % Coriolis matrix
for i = 1:4
    for j = 1:4
        for k = 1:4
            % Gamma(i,j,k) = Gamma(i,k,j)
            if k-j >= 0   % Compute
                ndx_chck = (i == j) + 2*(i == k) + 3*(j == k);
                switch(ndx_chck)
                    case 0  % No equal subscripts -> no simplifications
                        Gamma(i,j,k) = simplify((1/2)*sum(diag(jacobian([M(i,j);...
                            M(i,k);-M(k,j)],q_vect([k;j;i])))));
                    case 1  % i = j
                        Gamma(i,j,k) = simplify((1/2)*gradient(M(j,j),q_vect(k)));
                    case 2  % i = k
                        Gamma(i,j,k) = simplify((1/2)*gradient(M(k,k),q_vect(j)));
                    case 3  % j = k
                        Gamma(i,j,k) = simplify(sum([1,(1/2)]*diag(jacobian(...
                            [M(i,k);-M(k,k)],q_vect([k;i])))));
                    case 6  % i = j = k
                        Gamma(i,j,k) = simplify((1/2)*gradient(M(i,i),q_vect(i)));                    
                    otherwise
                        error(['The Jacobian switch-statement has been allowed', ... 
                        'to hit an exception. Inspect the corresponding code.']);
                end
            else    % Copy values based on symmetry: Gamma(i,j,k) = Gamma(i,k,j)
                Gamma(i,j,k) = Gamma(i,k,j);
            end
        end
    end
    % Compute the Coriolis matrix
    C(i,:) = simplify(squeeze(Gamma(i,:,:))*w_vect);
end

% Vectorized method of computing the Coriolis matrix 
% C_2 = sym(zeros(4,4));
% w_mat = blkdiag(w_vect.',w_vect.',w_vect.',w_vect.');
% Gamma_mat = [squeeze(Gamma(1,:,:));squeeze(Gamma(2,:,:));squeeze(Gamma(3,:,:));squeeze(Gamma(4,:,:))];
% C_2 = simplify(w_mat*Gamma_mat);

%% Partial differential equation for the 4DOF gyroscope
diff_eq = T_vect - M*a_vect-simplify(C*w_vect);

display(['See Gyroscope Dynamics Manual for reference!'])
display(['Dynamics are in the format: M(q)\ddot{q} + C(q,\dot{q})\dot{q} = T'])
display(['For matrices: M and C; and generalized coordinates:'])
display(['  \ddot{q} = a_vect'])
display(['  \dot{q} = w_vect'])
display(['  q = q_vect'])
display(['  T = T_vect'])
display([' '])
display(['The variable "diff_eq" provides the quations: T - M(q)\ddot{q} + C(q,\dot{q})\dot{q} = 0.'])








































